package me.mathiaseklund.pitpvp.commands;

import org.bukkit.Bukkit;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

import me.mathiaseklund.pitpvp.Main;
import me.mathiaseklund.pitpvp.PlayerData;
import me.mathiaseklund.pitpvp.utils.Util;

public class BalanceCommand implements CommandExecutor {

	Main main = Main.getMain();
	Util util = main.getUtil();

	@Override
	public boolean onCommand(CommandSender sender, Command arg1, String arg2, String[] args) {
		if (args.length == 0) {
			if (sender instanceof Player) {
				Player player = (Player) sender;
				PlayerData data = main.getPlayers().getData(player);
				if (data.getLoaded()) {
					double bal = data.getGameCurrency();
					util.message(sender, main.getMessages().selfGameBalance.replace("%bal%", util.formatMoney(bal)));
				} else {
					util.error(sender, "Your data is not loaded yet, try again later.");
				}
			} else {
				util.error("Command can only be executed by a player");
			}
		} else {
			if (sender.isOp()) {
				// check other players balance.
				String name = args[0];
				Player target = Bukkit.getPlayer(name);
				if (target != null) {
					PlayerData data = main.getPlayers().getData(target);
					if (data.getLoaded()) {
						double bal = data.getGameCurrency();
						util.message(sender, main.getMessages().otherGameBalance.replace("%bal%", util.formatMoney(bal))
								.replace("%target%", target.getName()));
					} else {
						util.error(sender, target.getName() + "'s data is not loaded yet, try again later.");
					}
				} else {
					util.error(sender, "Target player is not online.");
				}
			}
		}
		return false;
	}

}
