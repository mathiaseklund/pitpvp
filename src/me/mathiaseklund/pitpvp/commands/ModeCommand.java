package me.mathiaseklund.pitpvp.commands;

import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

import me.mathiaseklund.pitpvp.Main;
import me.mathiaseklund.pitpvp.PlayerData;
import me.mathiaseklund.pitpvp.enums.Mode;
import me.mathiaseklund.pitpvp.utils.Util;

public class ModeCommand implements CommandExecutor {
	Main main = Main.getMain();
	Util util = main.getUtil();

	@Override
	public boolean onCommand(CommandSender sender, Command arg1, String arg2, String[] args) {
		if (args.length == 0) {
			util.message(sender, "&eUSAGE:&7 /mode <casual/monetary/realmonetary>");
		} else if (args.length == 1) {
			if (sender instanceof Player) {
				Player player = (Player) sender;
				String m = args[0].toUpperCase();
				Mode mode;
				try {
					mode = Mode.valueOf(m);
					PlayerData data = main.getPlayers().getData(player);
					if (data.getSpawned()) {
						util.error(player,
								"Unable to change gamemode while in the arena. You can change your mode in the spawn area before dropping in.");
					} else {
						data.setMode(mode);

						util.message(player, "&eYou've set your Mode to " + mode.toString());
					}
				} catch (Exception e) {
					util.error(player, "Invalid Mode. Available Modes: Casual, Monetary, RealMonetary");
				}
			} else {
				util.error("Command can only be run by a player.");
			}
		}
		return false;
	}

}
