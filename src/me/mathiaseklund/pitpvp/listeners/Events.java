package me.mathiaseklund.pitpvp.listeners;

import org.bukkit.Bukkit;
import org.bukkit.Material;
import org.bukkit.Sound;
import org.bukkit.attribute.Attribute;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.EventPriority;
import org.bukkit.event.Listener;
import org.bukkit.event.entity.EntityDamageByEntityEvent;
import org.bukkit.event.entity.EntityDeathEvent;
import org.bukkit.event.entity.EntityPickupItemEvent;
import org.bukkit.event.player.PlayerJoinEvent;
import org.bukkit.event.player.PlayerLoginEvent;
import org.bukkit.event.player.PlayerLoginEvent.Result;
import org.bukkit.event.player.PlayerMoveEvent;
import org.bukkit.event.player.PlayerQuitEvent;
import org.bukkit.inventory.ItemStack;

import me.mathiaseklund.pitpvp.Main;
import me.mathiaseklund.pitpvp.PlayerData;
import me.mathiaseklund.pitpvp.enums.Mode;
import me.mathiaseklund.pitpvp.utils.Util;

public class Events implements Listener {

	Main main = Main.getMain();
	Util util = main.getUtil();

	@EventHandler(priority = EventPriority.MONITOR)
	public void onPlayerLogin(PlayerLoginEvent event) {
		if (event.getResult() == Result.ALLOWED) {

			Bukkit.getScheduler().runTaskAsynchronously(main, new Runnable() {
				public void run() {
					main.getPlayers().loadPlayer(event.getPlayer());
				}
			});
		}
	}

	@EventHandler(priority = EventPriority.LOWEST)
	public void checkReady(PlayerLoginEvent event) {
		if (!main.getReady()) {
			event.setKickMessage("Server is not ready yet. Try again soon!");
			event.setResult(Result.KICK_OTHER);
		}
	}

	@EventHandler
	public void onPlayerJoin(PlayerJoinEvent event) {
		event.getPlayer().setHealth(event.getPlayer().getAttribute(Attribute.GENERIC_MAX_HEALTH).getBaseValue());
		event.getPlayer().setFoodLevel(20);
		event.getPlayer().getInventory().clear();
		event.getPlayer().teleport(main.getCfg().getSpawn());
	}

	@EventHandler
	public void onPlayerQuit(PlayerQuitEvent event) {
		event.getPlayer().getInventory().clear();
		main.getPlayers().unloadPlayer(event.getPlayer());
	}

	@EventHandler(priority = EventPriority.MONITOR)
	public void onPlayerMove(PlayerMoveEvent event) {
		Bukkit.getScheduler().runTaskAsynchronously(main, new Runnable() {
			public void run() {
				PlayerData data = main.getPlayers().getData(event.getPlayer());
				if (data != null) {
					if (!data.getSpawned()) {
						boolean canSpawn = true;
						int spawnY = main.getCfg().getSpawnY();
						if (event.getTo().getBlockY() == (spawnY - 1)) {
							Mode mode = data.getMode();
							if (mode == Mode.CASUAL) {
								if (data.getGameCurrency() >= main.getCfg().getCasualFee()) {
									// data.takeGameCurrency(main.getCfg().getCasualFee());
								} else {
									canSpawn = false;
									util.error(event.getPlayer(),
											"You are unable to jump in because you don't have enough $ Balance!");
								}
							} else if (mode == Mode.MONETARY) {
								if (data.getGameCurrency() >= main.getCfg().getMonetaryFee()) {
									// data.takeGameCurrency(main.getCfg().getMonetaryFee());
								} else {
									canSpawn = false;
									util.error(event.getPlayer(),
											"You are unable to jump in because you don't have enough $ Balance!");
								}
							} else if (mode == Mode.REALMONETARY) {
								if (data.getRealCurrency() >= main.getCfg().getRealMonetaryFee()) {
									// data.takeGameCurrency(main.getCfg().getRealMonetaryFee());
								} else {
									canSpawn = false;
									util.error(event.getPlayer(),
											"You are unable to jump in because you don't have enough Coin Balance!");
								}
							}

							if (canSpawn) {

								data.setSpawned(true);

								addEquipment(event.getPlayer());

							} else {
								Bukkit.getScheduler().runTask(main, new Runnable() {
									public void run() {
										event.getPlayer().teleport(event.getPlayer().getWorld().getSpawnLocation());
									}
								});
							}
						}

					}
				} else {
					event.setCancelled(true);
				}
			}
		});
	}

	@EventHandler
	public void onItemPickup(EntityPickupItemEvent event) {
		if (event.getEntity() instanceof Player) {
			Player player = (Player) event.getEntity();
			if (player.hasPermission("pitpvp.ignorebad")) {
				ItemStack is = event.getItem().getItemStack();
				if (hasWorse(player, is)) {
					event.setCancelled(true);
					event.getItem().remove();
					replaceWorse(player, is);
				} else {
					event.setCancelled(true);
				}

			}

		} else {
			event.setCancelled(true);
		}
	}

	@EventHandler
	public void onEntityDeath(EntityDeathEvent event) {
		if (event.getEntity() instanceof Player) {
			Player player = (Player) event.getEntity();
			PlayerData data = main.getPlayers().getData(player);
			data.setSpawned(false);
			if (event.getEntity().getKiller() != null) {
				Player killer = event.getEntity().getKiller();

				PlayerData killerData = main.getPlayers().getData(killer);
				switch (data.getMode()) {
				case CASUAL:
					data.takeGameCurrency(main.getCfg().getCasualFee());
					killerData.giveGameCurrency(main.getCfg().getCasualFee());
					break;
				case MONETARY:
					data.takeGameCurrency(main.getCfg().getMonetaryFee());
					killerData.giveGameCurrency(main.getCfg().getMonetaryFee());
					break;
				case REALMONETARY:
					data.takeGameCurrency(main.getCfg().getRealMonetaryFee());
					killerData.giveGameCurrency((main.getCfg().getRealMonetaryFee() * 0.9));
					break;
				default:
					data.takeGameCurrency(main.getCfg().getCasualFee());
					killerData.giveGameCurrency(main.getCfg().getCasualFee());
					break;
				}
			}
		}

	}

	@EventHandler
	public void onEntityDamageByEntity(EntityDamageByEntityEvent event) {
		if (event.getEntity() instanceof Player && event.getDamager() instanceof Player) {
			Player attacked = (Player) event.getEntity();

			PlayerData aData = main.getPlayers().getData(attacked);
			if (aData != null) {
				if (!aData.getSpawned()) {
					event.setCancelled(true);
				}
			} else {
				event.setCancelled(true);
			}
		}
	}

	// METHODS

	public boolean hasWorse(Player player, ItemStack is) {

		String newtype = is.getType().toString();

		boolean worse = false;
		if (newtype.contains("SWORD")) {
			// WEAPON
			boolean found = false;
			for (int i = 0; i < player.getInventory().getSize(); i++) {
				ItemStack item = player.getInventory().getItem(i);
				if (item != null) {
					String type = item.getType().toString();
					if (newtype.contains("WOODEN")) {
						if (type.contains("SWORD")) {
							found = true;
							worse = false;
							break;
						}
					} else if (newtype.contains("STONE")) {
						if (type.contains("SWORD")) {
							found = true;
							if (type.contains("WOODEN")) {
								worse = true;
								break;
							}
						}
					} else if (newtype.contains("GOLDEN_SWORD")) {
						if (type.contains("SWORD")) {
							found = true;
							if (type.contains("WOODEN") || type.contains("STONE")) {

								worse = true;
								break;
							}
						}
					} else if (newtype.contains("IRON_SWORD")) {
						if (type.contains("SWORD")) {
							found = true;
							if (type.contains("WOODEN") || type.contains("STONE") || type.contains("GOLDEN")) {

								worse = true;
								break;
							}
						}
					} else if (newtype.contains("DIAMOND_SWORD")) {
						if (type.contains("SWORD")) {
							found = true;
							if (type.contains("WOODEN") || type.contains("STONE") || type.contains("GOLDEN")
									|| type.contains("IRON")) {

								worse = true;
								break;
							}
						}
					} else if (newtype.contains("NETHERITE_SWORD")) {
						if (type.contains("SWORD")) {
							found = true;
							if (type.contains("WOODEN") || type.contains("STONE") || type.contains("GOLDEN")
									|| type.contains("IRON") || type.contains("DIAMOND")) {

								worse = true;
								break;
							}
						}
					}
				}
			}
			if (!found) {
				worse = true;
			}
		} else if (newtype.contains("HELMET") || newtype.contains("CHESTPLATE") || newtype.contains("LEGGINGS")
				|| newtype.contains("BOOTS")) {
			boolean cont = true;
			if (newtype.contains("HELMET")) {
				if (player.getInventory().getHelmet() == null) {
					return true;
				}
			} else if (newtype.contains("CHESTPLATE")) {
				if (player.getInventory().getChestplate() == null) {
					return true;
				}
			} else if (newtype.contains("LEGGINGS")) {
				if (player.getInventory().getLeggings() == null) {
					return true;
				}
			} else if (newtype.contains("BOOTS")) {
				if (player.getInventory().getBoots() == null) {
					return true;
				}
			}
			if (cont) {
				switch (newtype) {
				case "LEATHER_HELMET":
					if (player.getInventory().getHelmet() == null) {
						worse = true;
					}
					break;
				case "LEATHER_CHESTPLATE":
					if (player.getInventory().getChestplate() == null) {
						worse = true;
					}
					break;
				case "LEATHER_LEGGINGS":
					if (player.getInventory().getLeggings() == null) {
						worse = true;
					}
					break;
				case "LEATHER_BOOTS":
					if (player.getInventory().getBoots() == null) {
						worse = true;
					}
					break;
				case "CHAINMAIL_HELMET":
					String type = player.getInventory().getHelmet().getType().toString();
					if (type.contains("LEATHER")) {
						worse = true;
					}
					break;
				case "GOLDEN_HELMET":
					type = player.getInventory().getHelmet().getType().toString();
					if (type.contains("LEATHER") || type.contains("CHAINMAIL")) {
						worse = true;
					}
					break;
				case "IRON_HELMET":
					type = player.getInventory().getHelmet().getType().toString();
					if (type.contains("LEATHER") || type.contains("CHAINMAIL") || type.contains("GOLDEN")) {
						worse = true;
					}
					break;
				case "DIAMOND_HELMET":
					type = player.getInventory().getHelmet().getType().toString();
					if (type.contains("LEATHER") || type.contains("CHAINMAIL") || type.contains("GOLDEN")
							|| type.contains("IRON")) {
						worse = true;
					}
					break;
				case "NETHERITE_HELMET":
					type = player.getInventory().getHelmet().getType().toString();
					if (type.contains("LEATHER") || type.contains("CHAINMAIL") || type.contains("GOLDEN")
							|| type.contains("IRON") || type.contains("DIAMOND")) {
						worse = true;
					}
					break;
				case "CHAINMAIL_CHESTPLATE":
					type = player.getInventory().getChestplate().getType().toString();
					if (type.contains("LEATHER")) {
						worse = true;
					}
					break;
				case "GOLDEN_CHESTPLATE":
					type = player.getInventory().getChestplate().getType().toString();
					if (type.contains("LEATHER") || type.contains("CHAINMAIL")) {
						worse = true;
					}
					break;
				case "IRON_CHESTPLATE":
					type = player.getInventory().getChestplate().getType().toString();
					if (type.contains("LEATHER") || type.contains("CHAINMAIL") || type.contains("GOLDEN")) {
						worse = true;
					}
					break;
				case "DIAMOND_CHESTPLATE":
					type = player.getInventory().getChestplate().getType().toString();
					if (type.contains("LEATHER") || type.contains("CHAINMAIL") || type.contains("GOLDEN")
							|| type.contains("IRON")) {
						worse = true;
					}
					break;
				case "NETHERITE_CHESTPLATE":
					type = player.getInventory().getChestplate().getType().toString();
					if (type.contains("LEATHER") || type.contains("CHAINMAIL") || type.contains("GOLDEN")
							|| type.contains("IRON") || type.contains("DIAMOND")) {
						worse = true;
					}
					break;
				case "CHAINMAIL_LEGGINGS":
					type = player.getInventory().getLeggings().getType().toString();
					if (type.contains("LEATHER")) {
						worse = true;
					}
					break;
				case "GOLDEN_LEGGINGS":
					type = player.getInventory().getLeggings().getType().toString();
					if (type.contains("LEATHER") || type.contains("CHAINMAIL")) {
						worse = true;
					}
					break;
				case "IRON_LEGGINGS":
					type = player.getInventory().getLeggings().getType().toString();
					if (type.contains("LEATHER") || type.contains("CHAINMAIL") || type.contains("GOLDEN")) {
						worse = true;
					}
					break;
				case "DIAMOND_LEGGINGS":
					type = player.getInventory().getLeggings().getType().toString();
					if (type.contains("LEATHER") || type.contains("CHAINMAIL") || type.contains("GOLDEN")
							|| type.contains("IRON")) {
						worse = true;
					}
					break;
				case "NETHERITE_LEGGINGS":
					type = player.getInventory().getLeggings().getType().toString();
					if (type.contains("LEATHER") || type.contains("CHAINMAIL") || type.contains("GOLDEN")
							|| type.contains("IRON") || type.contains("DIAMOND")) {
						worse = true;
					}
					break;
				case "CHAINMAIL_BOOTS":
					type = player.getInventory().getBoots().getType().toString();
					if (type.contains("LEATHER")) {
						worse = true;
					}
					break;
				case "GOLDEN_BOOTS":
					type = player.getInventory().getBoots().getType().toString();
					if (type.contains("LEATHER") || type.contains("CHAINMAIL")) {
						worse = true;
					}
					break;
				case "IRON_BOOTS":
					type = player.getInventory().getBoots().getType().toString();
					if (type.contains("LEATHER") || type.contains("CHAINMAIL") || type.contains("GOLDEN")) {
						worse = true;
					}
					break;
				case "DIAMOND_BOOTS":
					type = player.getInventory().getBoots().getType().toString();
					if (type.contains("LEATHER") || type.contains("CHAINMAIL") || type.contains("GOLDEN")
							|| type.contains("IRON")) {
						worse = true;
					}
					break;
				case "NETHERITE_BOOTS":
					type = player.getInventory().getBoots().getType().toString();
					if (type.contains("LEATHER") || type.contains("CHAINMAIL") || type.contains("GOLDEN")
							|| type.contains("IRON") || type.contains("DIAMOND")) {
						worse = true;
					}
					break;
				default:
					worse = true;
					break;
				}
			}
		}

		return worse;

	}

	public void addEquipment(Player player) {
		player.getInventory().clear();
		player.getInventory().addItem(new ItemStack(Material.WOODEN_SWORD));
		player.getInventory().setHelmet(new ItemStack(Material.LEATHER_HELMET));
		player.getInventory().setChestplate(new ItemStack(Material.LEATHER_CHESTPLATE));
		player.getInventory().setLeggings(new ItemStack(Material.LEATHER_LEGGINGS));
		player.getInventory().setBoots(new ItemStack(Material.LEATHER_BOOTS));

	}

	public void replaceWorse(Player player, ItemStack is) {
		String type = is.getType().toString();
		if (type.contains("HELMET")) {
			if (player.getInventory().getHelmet() != null) {
				player.getWorld().dropItem(player.getLocation(), player.getInventory().getHelmet());
			}
			player.getInventory().setHelmet(is);
		} else if (type.contains("CHESTPLATE")) {
			if (player.getInventory().getChestplate() != null) {
				player.getWorld().dropItem(player.getLocation(), player.getInventory().getChestplate());
			}
			player.getInventory().setChestplate(is);
		} else if (type.contains("LEGGINGS")) {
			if (player.getInventory().getLeggings() != null) {
				player.getWorld().dropItem(player.getLocation(), player.getInventory().getLeggings());
			}
			player.getInventory().setLeggings(is);
		} else if (type.contains("BOOTS")) {
			if (player.getInventory().getBoots() != null) {
				player.getWorld().dropItem(player.getLocation(), player.getInventory().getBoots());
			}
			player.getInventory().setBoots(is);
		} else if (type.contains("SWORD")) {
			int slot = 0;
			for (int i = 0; i < player.getInventory().getSize(); i++) {
				ItemStack item = player.getInventory().getItem(i);
				if (item != null) {
					if (item.getType() != Material.AIR) {
						if (item.getType().toString().contains("SWORD")) {
							slot = i;
							break;
						}
					}
				}
			}
			if (player.getInventory().getItem(slot) != null) {
				player.getWorld().dropItem(player.getLocation(), player.getInventory().getItem(slot));
			}
			player.getInventory().setItem(slot, is);
		} else {
			player.getInventory().addItem(is);
		}
		player.playSound(player.getLocation(), Sound.ENTITY_ITEM_PICKUP, 1, 1);
	}
}
