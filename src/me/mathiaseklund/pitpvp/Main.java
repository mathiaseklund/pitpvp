package me.mathiaseklund.pitpvp;

import java.io.File;

import org.bukkit.plugin.java.JavaPlugin;

import me.mathiaseklund.pitpvp.commands.BalanceCommand;
import me.mathiaseklund.pitpvp.commands.ModeCommand;
import me.mathiaseklund.pitpvp.commands.RealBalanceCommand;
import me.mathiaseklund.pitpvp.listeners.Events;
import me.mathiaseklund.pitpvp.utils.Config;
import me.mathiaseklund.pitpvp.utils.Messages;
import me.mathiaseklund.pitpvp.utils.SQL;
import me.mathiaseklund.pitpvp.utils.Util;

public class Main extends JavaPlugin {

	static Main main;

	Config config;
	Util util;
	Messages messages;
	Players players;
	SQL sql;
	boolean ready = false;

	public static Main getMain() {
		return main;
	}

	public void onEnable() {
		main = this;
		loadFiles();

		players = new Players();

		loadListeners();
		loadCommands();
	}

	void loadListeners() {
		getServer().getPluginManager().registerEvents(new Events(), this);
	}

	void loadCommands() {
		getCommand("balance").setExecutor(new BalanceCommand());
		getCommand("realbalance").setExecutor(new RealBalanceCommand());
		getCommand("mode").setExecutor(new ModeCommand());
	}

	void loadFiles() {
		File f = new File(getDataFolder(), "config.yml");
		if (!f.exists()) {
			saveResource("config.yml", true);
		}
		config = new Config(f);

		f = new File(getDataFolder(), "messages.yml");
		if (!f.exists()) {
			saveResource("messages.yml", true);
		}
		messages = new Messages(f);

		util = new Util();

		sql = new SQL();
	}

	// GETTERS //

	public Config getCfg() {
		return config;
	}

	public Util getUtil() {
		return util;
	}

	public Messages getMessages() {
		return messages;
	}

	public SQL getSql() {
		return sql;
	}

	public Players getPlayers() {
		return players;
	}

	public boolean getReady() {
		return ready;
	}

	public void setReady(boolean b) {
		ready = b;
	}
}
