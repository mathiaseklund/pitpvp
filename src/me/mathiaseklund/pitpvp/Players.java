package me.mathiaseklund.pitpvp;

import java.util.HashMap;

import org.bukkit.Bukkit;
import org.bukkit.entity.Player;

import me.mathiaseklund.pitpvp.utils.Config;

public class Players {

	Main main;
	Config config;

	HashMap<String, PlayerData> players;
	HashMap<Integer, PlayerData> playersById;

	public Players() {
		main = Main.getMain();
		config = main.getCfg();
		players= new HashMap<String, PlayerData>();
		playersById = new HashMap<Integer, PlayerData>();
		loadOnlinePlayers(this);
	}

	void loadOnlinePlayers(Players ps) {
		Bukkit.getScheduler().runTaskAsynchronously(main, new Runnable() {
			public void run() {
				for (Player p : Bukkit.getOnlinePlayers()) {
					PlayerData data = new PlayerData(ps, p);
					players.put(p.getName(), data);
				}
			}
		});
	}

	public void loadPlayer(Player player) {
		PlayerData data = new PlayerData(this, player);
		players.put(player.getName(), data);
	}

	public void unloadPlayer(Player player) {
		PlayerData data = getData(player);
		int id = data.getId();

		players.remove(player.getName());
		playersById.remove(id);
	}

	// GETTERS //

	public PlayerData getData(Player player) {
		return players.get(player.getName());
	}

	public PlayerData getData(int id) {
		return playersById.get(id);
	}

	// SETTERS //

	public void setPlayerDataById(int id, PlayerData data) {
		playersById.put(id, data);
	}
}
