package me.mathiaseklund.pitpvp.utils;

import java.io.File;

import org.bukkit.configuration.file.FileConfiguration;
import org.bukkit.configuration.file.YamlConfiguration;

import me.mathiaseklund.pitpvp.Main;

public class Messages {

	Main main;
	File f;
	FileConfiguration fc;

	public String loadingData, loadedData, selfGameBalance, otherGameBalance, selfRealBalance, otherRealBalance;

	public Messages(File f) {
		main = Main.getMain();
		this.f = f;
		load();
	}

	void load() {
		fc = YamlConfiguration.loadConfiguration(f);

		loadingData = fc.getString("player.loadingData", "&cLoading Data...");
		loadedData = fc.getString("player.loadedData", "&cFinished Loading Data..");
		selfGameBalance = fc.getString("player.gamebalance.self");
		otherGameBalance = fc.getString("player.gamebalance.other");
		selfRealBalance = fc.getString("player.realbalance.self");
		otherRealBalance = fc.getString("player.realbalance.other");
	}
}
