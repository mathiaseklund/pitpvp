package me.mathiaseklund.pitpvp.utils;

import java.text.NumberFormat;
import java.util.Locale;

import org.bukkit.Bukkit;
import org.bukkit.command.CommandSender;

import me.mathiaseklund.pitpvp.Main;
import net.md_5.bungee.api.ChatColor;

public class Util {

	Main main;
	Config config;

	public Util() {
		main = Main.getMain();
		config = main.getCfg();
	}

	public void debug(String msg) {
		if (config.getDebug()) {
			debug(Bukkit.getConsoleSender(), msg);
		}
	}

	public void debug(CommandSender sender, String msg) {
		if (config.getDebug()) {
			String prefix = "&7[&eDEBUG&7]&r ";
			sender.sendMessage(ChatColor.translateAlternateColorCodes('&', prefix + msg));
		}
	}

	public void info(String msg) {
		info(Bukkit.getConsoleSender(), msg);
	}

	public void info(CommandSender sender, String msg) {
		String prefix = "&7[&aINFO&7]&r ";
		sender.sendMessage(ChatColor.translateAlternateColorCodes('&', prefix + msg));
	}

	public void error(String msg) {
		error(Bukkit.getConsoleSender(), msg);
	}

	public void error(CommandSender sender, String msg) {
		String prefix = "&7[&cERROR&7]&r ";
		sender.sendMessage(ChatColor.translateAlternateColorCodes('&', prefix + msg));
	}

	public void message(CommandSender sender, String msg) {
		sender.sendMessage(ChatColor.translateAlternateColorCodes('&', msg));
	}

	public String formatMoney(double value) {
		String output = NumberFormat.getNumberInstance(Locale.US).format(value);
		return output;
	}
}
