package me.mathiaseklund.pitpvp.utils;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

import org.bukkit.Bukkit;
import org.bukkit.entity.Player;

import me.mathiaseklund.pitpvp.Main;
import me.mathiaseklund.pitpvp.PlayerData;

public class SQL {

	Main main;
	Util util;
	Config config;

	String host, user, pass, db;
	int port;

	Connection conn;

	public SQL() {
		main = Main.getMain();
		util = main.getUtil();
		config = main.getCfg();
		Bukkit.getScheduler().runTaskAsynchronously(main, new Runnable() {
			public void run() {
				setupSql();
			}
		});
	}

	void setupSql() {
		util.info("Setting up SQL Database");
		host = config.getHost();
		user = config.getUser();
		pass = config.getPass();
		db = config.getDb();
		port = config.getPort();

		try {
			openConnection();
			setupTables();
		} catch (ClassNotFoundException | SQLException e) {
			e.printStackTrace();
		}
	}

	void openConnection() throws SQLException, ClassNotFoundException {
		if (conn != null && !conn.isClosed()) {
			return;
		}

		synchronized (this) {
			if (conn != null && !conn.isClosed()) {
				return;
			}
			Class.forName("com.mysql.jdbc.Driver");
			conn = DriverManager.getConnection("jdbc:mysql://" + host + ":" + port + "/" + db, user, pass);
		}
	}

	void setupTables() {
		try {
			if (conn != null && !conn.isClosed()) {
				PreparedStatement stmt = conn.prepareStatement(
						"CREATE TABLE IF NOT EXISTS users (" + "id INT UNSIGNED AUTO_INCREMENT PRIMARY KEY,"
								+ "name VARCHAR(30) NOT NULL," + "uuid VARCHAR(64) NOT NULL,"
								+ "gameCurrency DOUBLE DEFAULT 100," + "realCurrency DOUBLE DEFAULT 0,"
								+ "reg_date TIMESTAMP DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP" + ");");
				stmt.execute();
				stmt.close();

				conn.close();
				util.info("Finished setting up SQL Database");
				main.setReady(true);
			}
		} catch (SQLException e) {
			e.printStackTrace();
		}
	}

	public void loadPlayerData(PlayerData data) {
		Player player = data.getPlayer();
		String name = player.getName();
		util.info("Loading PlayerData for " + name);
		String uuid = player.getUniqueId().toString();

		try {
			openConnection();

			PreparedStatement stmt = conn
					.prepareStatement("SELECT id, gameCurrency, realCurrency FROM users WHERE uuid=? LIMIT 1");
			stmt.setString(1, uuid);
			ResultSet result = stmt.executeQuery();
			if (result.next()) {
				int id = result.getInt("id");
				double gameCurrency = result.getDouble("gameCurrency");
				double realCurrency = result.getDouble("realCurrency");

				data.setData(id, gameCurrency, realCurrency);
				result.close();
				stmt.close();
				conn.close();
			} else {
				// player not found
				result.close();
				stmt.close();

				stmt = conn.prepareStatement("INSERT INTO users (name, uuid) VALUES (?, ?);");
				stmt.setString(1, name);
				stmt.setString(2, uuid);
				stmt.execute();
				stmt.close();

				stmt = conn.prepareStatement("SELECT id, gameCurrency, realCurrency FROM users WHERE uuid=? LIMIT 1");
				stmt.setString(1, uuid);
				result = stmt.executeQuery();
				if (result.next()) {
					int id = result.getInt("id");
					double gameCurrency = result.getDouble("gameCurrency");
					double realCurrency = result.getDouble("realCurrency");

					data.setData(id, gameCurrency, realCurrency);
					result.close();
					stmt.close();
					conn.close();
				} else {
					util.error("Unable to load data for player " + name);
					result.close();
					stmt.close();
					conn.close();
				}
			}
		} catch (ClassNotFoundException | SQLException e) {
			e.printStackTrace();
		}

	}

	public void updateRealCurrency(PlayerData data) {
		int id = data.getId();
		double bal = data.getRealCurrency();

		try {
			openConnection();

			PreparedStatement stmt = conn.prepareStatement("UPDATE users SET realCurrency=? WHERE id=?");
			stmt.setDouble(1, bal);
			stmt.setInt(2, id);
			stmt.execute();
			stmt.close();
		} catch (ClassNotFoundException | SQLException e) {
			e.printStackTrace();
		}
	}

	public void updateGameCurrency(PlayerData data) {
		int id = data.getId();
		double bal = data.getGameCurrency();

		try {
			openConnection();

			PreparedStatement stmt = conn.prepareStatement("UPDATE users SET gameCurrency=? WHERE id=?");
			stmt.setDouble(1, bal);
			stmt.setInt(2, id);
			stmt.execute();
			stmt.close();
		} catch (ClassNotFoundException | SQLException e) {
			e.printStackTrace();
		}
	}
}
