package me.mathiaseklund.pitpvp.utils;

import java.io.File;
import java.io.IOException;

import org.bukkit.Bukkit;
import org.bukkit.Location;
import org.bukkit.configuration.file.FileConfiguration;
import org.bukkit.configuration.file.YamlConfiguration;

import me.mathiaseklund.pitpvp.Main;

public class Config {

	Main main;
	File f;
	FileConfiguration fc;

	boolean debug;

	String host, user, pass, db;
	int port, spawny;
	Location spawn;
	double casualFee, monetaryFee, realMonetaryFee;

	public Config(File f) {
		main = Main.getMain();
		this.f = f;

		load();
	}

	void load() {
		fc = YamlConfiguration.loadConfiguration(f);

		debug = fc.getBoolean("debug");

		// SQL
		host = fc.getString("sql.host");
		user = fc.getString("sql.user");
		pass = fc.getString("sql.pass");
		db = fc.getString("sql.db");
		port = fc.getInt("sql.port");

		// SPAWN
		String location = fc.getString("spawn");
		String[] lParts = location.split("@");
		spawn = new Location(Bukkit.getWorld(lParts[0]), Integer.parseInt(lParts[1]), Integer.parseInt(lParts[2]),
				Integer.parseInt(lParts[3]));
		Bukkit.getWorld(lParts[0]).setSpawnLocation(spawn);
		spawny = fc.getInt("spawn-y");

		casualFee = fc.getDouble("fee.casual");
		monetaryFee = fc.getDouble("fee.monetary");
		realMonetaryFee = fc.getDouble("fee.realmonetary");

	}

	public void save() {
		try {
			fc.save(f);
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	// GETTERS //

	public boolean getDebug() {
		return debug;
	}

	public String getHost() {
		return host;
	}

	public String getUser() {
		return user;
	}

	public String getPass() {
		return pass;
	}

	public String getDb() {
		return db;
	}

	public int getPort() {
		return port;
	}

	public int getSpawnY() {
		return spawny;
	}

	public Location getSpawn() {
		return spawn;
	}

	public double getCasualFee() {
		return casualFee;
	}

	public double getMonetaryFee() {
		return monetaryFee;
	}

	public double getRealMonetaryFee() {
		return realMonetaryFee;
	}

	// SETTERS //

	public void setDebug(boolean b) {
		debug = b;

		Bukkit.getScheduler().runTaskAsynchronously(main, new Runnable() {
			public void run() {
				fc.set("debug", b);
				save();
			}
		});
	}

}
