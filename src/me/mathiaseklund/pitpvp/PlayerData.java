package me.mathiaseklund.pitpvp;

import org.bukkit.Bukkit;
import org.bukkit.entity.Player;

import me.mathiaseklund.pitpvp.enums.Mode;
import me.mathiaseklund.pitpvp.utils.Config;
import me.mathiaseklund.pitpvp.utils.Util;
import net.md_5.bungee.api.ChatColor;
import net.md_5.bungee.api.ChatMessageType;
import net.md_5.bungee.api.chat.TextComponent;

public class PlayerData {

	Main main;
	Config config;
	Util util;
	Player player;
	boolean loaded = false, spawned = false;
	int id;
	double gameCurrency, realCurrency;
	Mode mode = Mode.CASUAL;
	PlayerData data;

	public PlayerData(Players players, Player player) {
		main = Main.getMain();
		config = main.getCfg();
		util = main.getUtil();
		this.player = player;
		this.data = this;
		load(this);
	}

	void load(PlayerData data) {
		util.message(player, main.getMessages().loadingData);
		main.getSql().loadPlayerData(data);
		hideOtherPlayers();
	}

	// SETTERS //

	public void setData(int id, double gameCurrency, double realCurrency) {
		this.id = id;
		this.gameCurrency = gameCurrency;
		this.realCurrency = realCurrency;

		main.getPlayers().setPlayerDataById(id, this);

		loaded = true;
		util.message(player, main.getMessages().loadedData);
		util.info("Loaded player data for " + player.getName());
	}

	public void setMode(Mode m) {
		mode = m;
		hideOtherPlayers();
	}

	public void setSpawned(boolean b) {
		spawned = b;
	}

	public void setGameCurrency(double d) {
		gameCurrency = d;

		Bukkit.getScheduler().runTaskAsynchronously(main, new Runnable() {
			public void run() {
				main.getSql().updateGameCurrency(data);
			}
		});
	}

	public void setRealCurrency(double d) {
		realCurrency = d;

		Bukkit.getScheduler().runTaskAsynchronously(main, new Runnable() {
			public void run() {
				main.getSql().updateRealCurrency(data);
			}
		});
	}

	// GETTERS //

	public Player getPlayer() {
		return player;
	}

	public boolean getLoaded() {
		return loaded;
	}

	public boolean getSpawned() {
		return spawned;
	}

	public int getId() {
		return id;
	}

	public double getGameCurrency() {
		return gameCurrency;
	}

	public double getRealCurrency() {
		return realCurrency;
	}

	public Mode getMode() {
		return mode;
	}

	// METHODS //

	void hideOtherPlayers() {
		for (Player p : Bukkit.getOnlinePlayers()) {
			if (p.getName() != player.getName()) {
				PlayerData data = main.getPlayers().getData(p);
				if (data.getMode() == mode) {
					// same mode, see eachother.
					p.showPlayer(main, player);
					player.showPlayer(main, p);
				} else {
					// hide eachother.
					p.hidePlayer(main, player);
					player.hidePlayer(main, p);
				}
			}
		}
	}

	// TODO add currency update messages.

	public void takeGameCurrency(double d) {
		setGameCurrency(getGameCurrency() - d);

		if (player != null) {
			String msg = ChatColor.translateAlternateColorCodes('&', "&4- &6$" + util.formatMoney(d));
			player.spigot().sendMessage(ChatMessageType.ACTION_BAR, new TextComponent(msg));
			util.message(player, msg);
		}
	}

	public void takeRealCurrency(double d) {
		setRealCurrency(getRealCurrency() - d);
		if (player != null) {
			String msg = ChatColor.translateAlternateColorCodes('&', "&4- &6" + util.formatMoney(d) + " Coins");
			player.spigot().sendMessage(ChatMessageType.ACTION_BAR, new TextComponent(msg));
			util.message(player, msg);
		}
	}

	public void giveGameCurrency(double d) {
		setGameCurrency(getGameCurrency() + d);
		if (player != null) {
			String msg = ChatColor.translateAlternateColorCodes('&', "&a+ &6$" + util.formatMoney(d));
			player.spigot().sendMessage(ChatMessageType.ACTION_BAR, new TextComponent(msg));
			util.message(player, msg);
		}
	}

	public void giveRealCurrency(double d) {
		setRealCurrency(getRealCurrency() + d);
		if (player != null) {
			String msg = ChatColor.translateAlternateColorCodes('&', "&a+ &6" + util.formatMoney(d) + " Coins");
			player.spigot().sendMessage(ChatMessageType.ACTION_BAR, new TextComponent(msg));
			util.message(player, msg);
		}
	}
}
